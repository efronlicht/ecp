use clipboard::{ClipboardContext, ClipboardProvider};
fn main() {
    let mut clip: ClipboardContext = ClipboardProvider::new().unwrap();
    let s = clip.get_contents().unwrap();
    print!("{}", s);
}
