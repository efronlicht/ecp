use clipboard::{ClipboardContext, ClipboardProvider};
use std::io::Read;
fn main() {
    let mut buf = Vec::with_capacity(0xFFFF);
    std::io::stdin().lock().read_to_end(&mut buf).unwrap();
    let len = buf.len();
    let mut clip: ClipboardContext = ClipboardProvider::new().unwrap();
    // safety: we never actually use the string for anything inside rust: we're just passing through to the clipboard.
    clip.set_contents(unsafe { String::from_utf8_unchecked(buf) })
        .unwrap_or_else(|err| panic!("failed to write {} bytes to clipboard: {}", len, err));
}
