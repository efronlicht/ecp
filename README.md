# ecp

read and write from the clipboard on the command-line, regardless of platform. works on windows, bsd, macos.

It even works in WSL: make sure to install it on the _windows_ side, but you can call it on the linux side.


## installation
- install rust and the cargo package manager via [rustup.rs](https://rustup.rs/)
-   
```sh
    cargo install ecopy epaste_ # note underscore
```
## installation (wsl)
## usage
note that on windows, these will be "ecopy.exe" and "epaste.exe".
```sh
+ echo "foo" | ecopy # stdin is copied to clipboard
+ epaste_ # dump clipboard to stdout
foo
```
